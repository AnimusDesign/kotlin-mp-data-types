rootProject.name = "datatypes"
enableFeaturePreview("GRADLE_METADATA")

pluginManagement {
  resolutionStrategy {
    eachPlugin {
      if (requested.id.id.startsWith("org.jetbrains.dokka")) {
        // Update Version Build Source if being changed.
        useVersion("1.4.20")
      }
      if (requested.id.id.startsWith("org.jetbrains.kotlin.") || requested.id.id.startsWith("org.jetbrains.kotlin.plugin.serialization")) {
        // Update Version Build Source if being changed.
        useVersion("1.4.30")
      }
      if (requested.id.id.startsWith("net.researchgate.release")) {
        // Update Version Build Source if being changed.
        useVersion("2.6.0")
      }
      if (
        requested.id.id.startsWith("org.gradle.kotlin.kotlin-dsl")
      ) {
        useVersion("2.0.0")
      }
    }
  }
  repositories {
    mavenLocal()
    mavenCentral()
    jcenter()
    gradlePluginPortal()
    maven { url = java.net.URI("https://dl.bintray.com/kotlin/kotlinx") }
    maven { url = java.net.URI("https://kotlin.bintray.com/kotlin-dev") }
    maven { url = uri("https://animusdesign-repository.appspot.com") }
  }
}
