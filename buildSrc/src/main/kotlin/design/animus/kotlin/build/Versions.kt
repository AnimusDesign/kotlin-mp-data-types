package design.animus.kotlin.build

object Versions {
    object Dependencies {
        const val kotlin = "1.4.32"
        const val serialization = "1.1.0"
        const val coroutine = "1.4.3"
        const val junit = "4.12"
    }
}
