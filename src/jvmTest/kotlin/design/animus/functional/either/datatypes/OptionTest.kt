package design.animus.functional.either.datatypes

import design.animus.functional.datatypes.either.startsWithCapital
import design.animus.functional.datatypes.option.Option
import kotlin.js.ExperimentalJsExport
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking

@ExperimentalJsExport
class OptionTest {
  @Test
  fun happyComprehension() {
    val a = Option.Some(1)
    val b = Option.Some(2)
    val rsp = Option.comprehension<Int> {
      val (possibleA) = a
      val (possibleB) = b
      possibleA + possibleB
    }
    assertTrue { rsp.isSome() }
    assertTrue { rsp.forceSome() == 3 }
  }

  @Test
  fun unHappyComprehension() {
    val a = Option.Some(1)
    val b = Option.Some(2)
    val c = Option.None<Int>()
    val rsp = Option.comprehension<Int> {
      val (possibleA) = a
      val (possibleB) = b
      val (possibleC) = c
      possibleA + possibleB
    }
    assertTrue { rsp.isNone() }
  }

  @Test
  fun testAsyncComprehension() = runBlocking {
    val rsp = Option.comprehensionAsync<String> {
      val a = GlobalScope.async { Option.Some("Hello") }
      val (data) = !a
      data
    }
    assertTrue { rsp.forceSome() == "Hello" }
  }

  @Test
  fun testAsyncComprehensionWithDefault() = runBlocking {
    val rsp = Option.comprehensionAsync<String>("Default") {
      val a = GlobalScope.async { Option.None<String>() }
      val (data) = !a
      data
    }
    assertTrue { rsp == "Default" }
  }
}

@ExperimentalJsExport
class StringExtensionTest {
  @Test
  fun testSuccess() {
    assertTrue { "Abc".startsWithCapital() }
  }

  @Test
  fun testFailure() {
    assertFalse { "abc".startsWithCapital() }
  }
}
