package design.animus.functional.datatypes.either

import kotlin.js.ExperimentalJsExport
import kotlin.js.JsExport

@ExperimentalJsExport
@JsExport
interface Error

@ExperimentalJsExport
@JsExport
data class BaseError(
  val cause: Throwable,
  val message: String,
  val code: Int = 0
) : Error
