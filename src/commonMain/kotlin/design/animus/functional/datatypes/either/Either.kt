package design.animus.functional.datatypes.either

import kotlin.js.ExperimentalJsExport

@ExperimentalJsExport
sealed class Either<out E, out R> {
  fun isRight() = this is Right
  fun isLeft() = this is Left

  fun forceRight(): R {
    val r = this as Right
    return r.right
  }

  fun forceLeft(): E {
    val l = this as Left
    return l.left
  }

  fun <RSP> fold(
    errorHandler: (E) -> RSP,
    handler: (R) -> RSP
  ): RSP {
    return when (this) {
      is Left<*> -> errorHandler(this.forceLeft() as E)
      is Right<*> -> handler(this.forceRight() as R)
    }
  }

  suspend fun <RSP> foldAsync(
    errorHandler: suspend (E) -> RSP,
    handler: suspend (R) -> RSP
  ): RSP {
    return when (this) {
      is Left<*> -> errorHandler(this.forceLeft() as E)
      is Right<*> -> handler(this.forceRight() as R)
    }
  }

  data class Left<E : Any>(val left: E) : Either<E, Nothing>()
  data class Right<R : Any>(val right: R) : Either<Nothing, R>()
  companion object {
    fun <E : Any> left(left: E): Either<E, Nothing> = Left(left)
    fun <R : Any> right(right: R): Either<Nothing, R> = Right(right)
    fun <E : Any, R : Any> comprehension(block: EitherComprehension<E, R>.() -> R): Either<E, R> {
      val state = EitherComprehension<E, R>()
      return try {
        Either.right(state.block())
      } catch (e: Exception) {
        val error = state.error as E
        left(error)
      }
    }

    fun <E : Any, R : Any> comprehension(
      castFrom: Either<E, R>,
      block: EitherComprehension<E, R>.() -> R
    ): Either<E, R> = comprehension<E, R>(block)

    suspend fun <E : Any, R : Any> comprehensionAsync(block: suspend EitherComprehensionAsync<E, R>.() -> R): Either<E, R> {
      val state = EitherComprehensionAsync<E, R>()
      return try {
        Either.right(state.block())
      } catch (e: Exception) {
        val error = state.error as E
        left(error)
      }
    }
  }
}
