package design.animus.functional.datatypes.either

import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext
import kotlin.js.ExperimentalJsExport
import kotlin.js.JsExport

suspend fun <R : Any> CoroutineScope.catchEitherAsync(
  ctx: CoroutineContext = Dispatchers.Default,
  block: suspend () -> R
): Deferred<Either<Error, R>> = coroutineScope {
  async(ctx) {
    try {
      Either.right(block())
    } catch (e: Exception) {
      Either.left<Error>(BaseError(cause = e, message = e.message ?: ""))
    }
  }
}

@ExperimentalJsExport
@JsExport
fun <R : Any> Either.Companion.catchEither(block: () -> R) = try {
  Either.right(block())
} catch (e: Exception) {
  Either.left<Error>(BaseError(cause = e, message = e.message ?: ""))
}
