@file:Suppress("UNCHECKED_CAST")

package design.animus.functional.datatypes.either

suspend fun <E : Any, R : Any, RSP : Any> List<Either<E, R>>.comprehendIfNoErrors(block: suspend (List<R>) -> RSP): Either<E, RSP> {
  val possibleErrors = this.filter { it.isLeft() }
  return if (possibleErrors.isNotEmpty()) {
    possibleErrors.first() as Either<E, RSP>
  } else {
    val a = this.map { it.forceRight() }
    Either.right(block(a))
  }
}

suspend fun <E : Any, R : Any> List<Either<E, R>>.foldValid(): Either<E, List<R>> {
  val possibleErrors = this.filter { it.isLeft() }
  return if (possibleErrors.isNotEmpty()) {
    possibleErrors.first() as Either<E, List<R>>
  } else {
    val a = this.map { it.forceRight() }
    Either.right(a)
  }
}
