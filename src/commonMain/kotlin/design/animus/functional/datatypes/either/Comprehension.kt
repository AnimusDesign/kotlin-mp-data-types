package design.animus.functional.datatypes.either

import design.animus.functional.datatypes.interfaces.IComprehension
import kotlinx.coroutines.Deferred
import kotlin.js.ExperimentalJsExport
import kotlin.js.JsExport

@ExperimentalJsExport
@JsExport
abstract class AEitherComprehension<E : Any, R : Any> : IComprehension<Any> {
  override var errorOccurred: Boolean = false
  override var error: Any? = null

  operator fun <E : Any, R : Any> Either<E, R>.component1() = when (this) {
    is Either.Left -> {
      errorOccurred = true
      error = this.left
      throw Exception()
    }
    is Either.Right -> this.right
  }

  fun <E : Any> errorOf(inError: E) {
    val rsp: Either<E, R> = Either.left(inError)
    errorOccurred = true
    error = inError
    throw Exception("")
  }
}

@ExperimentalJsExport
@JsExport
class EitherComprehension<E : Any, R : Any> : AEitherComprehension<E, R>()

class EitherComprehensionAsync<E : Any, R : Any> : AEitherComprehension<E, R>() {
  suspend operator fun <E : Any, R : Any> Deferred<Either<E, R>>.component1(): R = when (val future = this.await()) {
    is Either.Left -> {
      errorOccurred = true
      error = future.left
      throw Exception()
    }
    is Either.Right -> future.right
  }

  suspend operator fun <E : Any, R : Any> Deferred<Either<E, R>>.not() = this.await()
}

@ExperimentalJsExport
@JsExport
fun String.startsWithCapital() = Regex("""^[A-Z]""").containsMatchIn(this)
