package design.animus.functional.datatypes.interfaces

import kotlin.js.ExperimentalJsExport
import kotlin.js.JsExport

@ExperimentalJsExport
@JsExport
interface IComprehension<E : Any> {
  var errorOccurred: Boolean
  var error: E?
}
