package design.animus.functional.datatypes.option

import design.animus.functional.datatypes.interfaces.IComprehension
import kotlinx.coroutines.Deferred
import kotlin.js.ExperimentalJsExport
import kotlin.js.JsExport

@ExperimentalJsExport
@JsExport
abstract class AOptionComprehension<S : Any> : IComprehension<Boolean> {
  override var errorOccurred: Boolean = false
  override var error: Boolean? = false

  operator fun <I : Any> Option<I>.component1() = when (this) {
    is Option.None -> {
      errorOccurred = true
      error = true
      throw Exception()
    }
    is Option.Some -> this.item
  }
}

class OptionComprehension<S : Any> : AOptionComprehension<S>()

class OptionComprehensionAsync<S : Any> : AOptionComprehension<S>() {
  suspend operator fun <R : Any> Deferred<Option<R>>.not(): Option<R> {
    return this.await()
  }
}
