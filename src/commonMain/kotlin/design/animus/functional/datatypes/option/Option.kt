package design.animus.functional.datatypes.option

import kotlin.js.ExperimentalJsExport

@ExperimentalJsExport
sealed class Option<S : Any> {
  class None<I : Any> : Option<I>()
  data class Some<I : Any>(val item: I) : Option<I>()

  fun isSome() = this is Some
  fun isNone() = this is None

  fun forceSome(): S {
    val some = this as Some
    return some.item
  }

  fun forceNone(): None<Any> {
    return None<Any>()
  }

  suspend fun <R> fold(errorHandler: () -> R, handler: (S) -> R): R = when (this) {
    is None -> errorHandler()
    is Some -> handler(this.item)
  }

  suspend fun <R> foldRight(default: R, handler: (S) -> R): R = when (this) {
    is None -> default
    is Some -> handler(this.item)
  }

  companion object {
    fun <I : Any> comprehension(block: OptionComprehension<I>.() -> I): Option<I> {
      val state = OptionComprehension<I>()
      return try {
        Some(state.block())
      } catch (e: Exception) {
        None()
      }
    }

    fun <I : Any> comprehension(default: I, block: OptionComprehension<I>.() -> I): I {
      val state = OptionComprehension<I>()
      return try {
        state.block()
      } catch (e: Exception) {
        default
      }
    }

    suspend fun <I : Any> comprehensionAsync(block: suspend OptionComprehensionAsync<I>.() -> I): Option<I> {
      val state = OptionComprehensionAsync<I>()
      return try {
        Some(state.block())
      } catch (e: Exception) {
        None()
      }
    }

    suspend fun <I : Any> comprehensionAsync(default: I, block: suspend OptionComprehensionAsync<I>.() -> I): I {
      val state = OptionComprehensionAsync<I>()
      return try {
        state.block()
      } catch (e: Exception) {
        default
      }
    }
  }
}
