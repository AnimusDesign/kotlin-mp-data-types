import design.animus.kotlin.build.Versions.Dependencies
import java.net.URI

plugins {
  kotlin("multiplatform") version "1.4.32"
  id("org.jetbrains.dokka") version "1.4.20"
  id("net.researchgate.release")
  id("maven-publish")
  kotlin("plugin.serialization")
  id("org.jlleitschuh.gradle.ktlint") version "10.0.0"
}

group = "design.animus.kotlin.mp"

repositories {
  maven { url = URI("https://animusdesign-repository.appspot.com") }
  mavenCentral()
  jcenter()
  google()
  maven { url = URI("https://kotlin.bintray.com/kotlinx") }
  maven { url = URI("https://dl.bintray.com/kotlin/kotlin-js-wrappers") }
  maven { url = URI("https://kotlin.bintray.com/kotlinx") }
  maven { url = URI("https://maven.google.com") }
  maven { url = URI("https://plugins.gradle.org/m2/") }
  maven { url = URI("http://dl.bintray.com/kotlin/kotlin-dev") }
  maven { url = URI("https://dl.bintray.com/kotlin/kotlin-eap") }
  mavenLocal()
}

kotlin {
  metadata { }
  jvm {
    compilations.all {
      kotlinOptions.jvmTarget = "11"
      kotlinOptions.apiVersion = "1.4"
    }
  }
  js(BOTH) {
    browser()
    nodejs()
    compilations.named("main") {
      kotlinOptions {
        metaInfo = true
        sourceMap = true
        verbose = true
        moduleKind = "umd"
      }
    }
  }
  val hostOs = System.getProperty("os.name")
  val isMingwX64 = hostOs.startsWith("Windows")
  val nativeTarget = when {
    hostOs == "Mac OS X" -> macosX64("macOS")
    hostOs == "Linux" -> linuxX64("linuxX64")
    isMingwX64 -> mingwX64("mingwX64")
    else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
  }
  sourceSets {
    val commonMain by getting {
      dependencies {
        api(kotlin("stdlib-common"))
        implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
        implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:${Dependencies.serialization}")
        implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${Dependencies.serialization}")
      }
    }
    val commonTest by getting {
      dependencies {
        implementation(kotlin("test-common"))
        implementation(kotlin("test-annotations-common"))
      }
    }
    val jvmMain by getting {
      dependencies {
        api(kotlin("stdlib"))
        api(kotlin("reflect"))
      }
    }
    val jvmTest by getting {
      dependencies {
        implementation(kotlin("test"))
        implementation(kotlin("test-junit"))
        implementation("junit:junit:${Dependencies.junit}")
      }
    }
    val jsMain by getting {
      dependencies {
        api(kotlin("stdlib-js"))
      }
    }
    val jsTest by getting {
      dependencies {
        implementation(kotlin("test-js"))
      }
    }
    val nativeMain by creating {
      dependsOn(commonMain)
    }
    val nativeTest by creating {
      dependsOn(commonMain)
    }
    when (nativeTarget.name) {
      "macOS" -> {
        val macOSMain by getting {
          dependsOn(nativeMain)
        }
        val macOSTest by getting {
          dependsOn(nativeTest)
        }
      }
      "mingwX64" -> {
        val mingwX64Main by getting {
          dependsOn(nativeMain)
        }
        val mingwX64Test by getting {
          dependsOn(nativeTest)
        }
      }
      "linuxX64" -> {
        val linuxX64Main by getting {
          dependsOn(nativeMain)
        }
        val linuxX64Test by getting {
          dependsOn(nativeTest)
        }
      }
    }
  }
}

configure<PublishingExtension> {
  repositories {
    maven {
      url = URI("https://gitlab.com/api/v4/projects/${System.getenv("CI_PROJECT_ID")}/packages/maven/")
      credentials(HttpHeaderCredentials::class.java) {
        name = "Job-Token"
        value = System.getenv("CI_JOB_TOKEN")
      }
      authentication {
        create<HttpHeaderAuthentication>("header")
      }
    }
  }
}

val proj = project
tasks {
  val jvmTest by getting(org.jetbrains.kotlin.gradle.targets.jvm.tasks.KotlinJvmTest::class) {
    this.useJUnit()
    this.reports.junitXml.isEnabled = true
  }
}

configure<org.jlleitschuh.gradle.ktlint.KtlintExtension> {
  debug.set(true)
  verbose.set(true)
  android.set(false)
  enableExperimentalRules.set(true)
  additionalEditorconfigFile.set(file("$rootDir/.editorconfig"))
}
